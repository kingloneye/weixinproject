/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2018-01-15 11:47:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `sysid` varchar(32) NOT NULL,
  `author` varchar(32) NOT NULL,
  `briefing` text,
  `classifyId` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `createtime` datetime DEFAULT NULL,
  `isComment` varchar(1) DEFAULT NULL,
  `isDisplay` varchar(1) DEFAULT NULL,
  `label` varchar(32) DEFAULT NULL,
  `modifyTime` varchar(20) DEFAULT NULL,
  `publishDatetime` varchar(20) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('40289f865ff1507f015ff15426970001', 'kinglone', '代码', '4028b8815fced709015fcedfe2b70001', '<pre class=\"language-java\"><code> List&lt;Map&lt;String, Object&gt;&gt; commentListMap = new ArrayList&lt;Map&lt;String,Object&gt;&gt;();\n        for(int i=0;i&lt;comments.size();i++) {\n            Map&lt;String,Object&gt; map = new HashMap&lt;String, Object&gt;();\n            String commentId = comments.get(i).getSysid();\n            Comment comment = commentService.get(commentId);\n            Article article = articleService.get(comment.getArticle().getSysid());\n            map.put(\"articleId\", article.getSysid());\n            map.put(\"commentContent\", comments.get(i).getContent());\n            map.put(\"articleTitle\", article.getTitle());\n            map.put(\"commentId\",commentId);\n            commentListMap.add(map);\n        }\n        request.setAttribute(\"commentListMap\",commentListMap);\n        </code></pre>', '2017-11-25 11:59:05', 'Y', 'N', null, '2017-11-25 12:00:14', '2017-11-25 12:00:14', 'king');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd4feeced0002', 'kinglone', '      我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。 ', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;&nbsp;<img src=\"../upload/blog/20171119/1511106940508.jpg\" width=\"404\" height=\"323\" /></span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>', '2017-11-19 23:56:37', 'Y', 'Y', null, '2017-11-19 23:56:37', '2017-11-19 23:56:37', '我们的生命之所以贫瘠');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd4feeced0011', 'kinglone', '      我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。 ', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;&nbsp;<img src=\"../upload/blog/20171119/1511106940508.jpg\" width=\"404\" height=\"323\" /></span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>', '2017-11-19 23:56:37', 'Y', 'Y', null, '2017-11-19 23:56:37', '2017-11-19 21:56:37', '我们的生命之所以贫瘠22');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd4feeced3223', 'kinglone', '      我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。 ', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;&nbsp;<img src=\"../upload/blog/20171119/1511106940508.jpg\" width=\"404\" height=\"323\" /></span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我们的生命之所以贫瘠，原因往往不是放弃了工作，便是因工作放弃了沉思：要不断地工作，也要不断地沉思。生命原是一个不知来自何处去至何方的奇迹，存在也是一个时空的偶然，我们需要不停的奋斗，来印证我们生命的真正存在。这样我们便须活跃我们的思维，点燃灵台的明灯，照亮我们该走的路，以便我们继续跋涉。生命也是需要不断跋涉的，不管昨日你有多少功绩，不管昨日你园圃里有多少花朵，那是属于昨日；若你一心沉湎于昨日的喜悦，就难享今日更清醇的欢欣。今日，一个新的开始，更需要我们前进，更需要我们去孕育。人生是一条永远走不完的旅程，需要生命的火把，直至成灰而泪尽。</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>\r\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;</span><span style=\"font-family: tahoma, ����;\">&nbsp;</span></p>', '2017-11-19 23:56:37', 'Y', 'Y', null, '2017-11-19 23:56:37', '2017-11-19 20:56:37', '我们的生命之所以贫瘠33');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd5022ac10004', 'kinglone', '       爱心是一片照射在冬日的阳光，它能让贫病交加的人感受到人间的温暖；爱心是广袤无垠沙漠中的一股请泉，它能使濒临绝境的人获得生存的希望；爱心是一首动人的歌谣，它能使心灰意冷的人获得精神的慰藉；爱心是一场久旱的甘霖，它能使悲观失望的人获得心灵的滋润。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;爱心是一片照射在冬日的阳光，它能让贫病交加的人感受到人间的温暖；爱心是广袤无垠沙漠中的一股请泉，它能使濒临绝境的人获得生存的希望；爱心是一首动人的歌谣，它能使心灰意冷的人获得精神的慰藉；爱心是一场久旱的甘霖，它能使悲观失望的人获得心灵的滋润。</span></p>\n<p><span style=\"font-family: tahoma, ����;\"><img src=\"../upload/blog/20171119/1511107152567.jpg\" width=\"279\" height=\"440\" /></span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 没有蓝天的深邃，可以有白云的飘逸；没有大海的壮阔，可以有小溪的优雅；没有原野的芬芳，可以有小草的翠绿！生活中没有旁观者的席位，我们总能找到自己的位置，自己的光源，自己的声音。我们有美的胸襟，我们才活得坦然；我们活得坦然，生活才给我们快乐的体验。&nbsp;</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 菲尔丁说：\"不好的书也像不好的朋友一样，可能会把你戕害。\"这话没错。但不必为此走向另一极端，夸大书籍对人的品格的影响。更多的情况是：好人读了坏书仍是好人，坏人读了好书仍是坏人。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">友情，是人生一笔受益匪浅的储蓄。这储蓄，是患难中的倾囊相助，是错误道路上的逆耳忠言，是跌倒时的一把真诚搀扶，是痛苦时抹去泪水的一缕春风。</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp;爱心是一片照射在冬日的阳光，它能让贫病交加的人感受到人间的温暖；爱心是广袤无垠沙漠中的一股请泉，它能使濒临绝境的人获得生存的希望；爱心是一首动人的歌谣，它能使心灰意冷的人获得精神的慰藉；爱心是一场久旱的甘霖，它能使悲观失望的人获得心灵的滋润。</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 没有蓝天的深邃，可以有白云的飘逸；没有大海的壮阔，可以有小溪的优雅；没有原野的芬芳，可以有小草的翠绿！生活中没有旁观者的席位，我们总能找到自己的位置，自己的光源，自己的声音。我们有美的胸襟，我们才活得坦然；我们活得坦然，生活才给我们快乐的体验。&nbsp;</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 菲尔丁说：\"不好的书也像不好的朋友一样，可能会把你戕害。\"这话没错。但不必为此走向另一极端，夸大书籍对人的品格的影响。更多的情况是：好人读了坏书仍是好人，坏人读了好书仍是坏人。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">友情，是人生一笔受益匪浅的储蓄。这储蓄，是患难中的倾囊相助，是错误道路上的逆耳忠言，是跌倒时的一把真诚搀扶，是痛苦时抹去泪水的一缕春风。</span><span style=\"font-family: tahoma, ����;\">&nbsp;爱心是一片照射在冬日的阳光，它能让贫病交加的人感受到人间的温暖；爱心是广袤无垠沙漠中的一股请泉，它能使濒临绝境的人获得生存的希望；爱心是一首动人的歌谣，它能使心灰意冷的人获得精神的慰藉；爱心是一场久旱的甘霖，它能使悲观失望的人获得心灵的滋润。</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 没有蓝天的深邃，可以有白云的飘逸；没有大海的壮阔，可以有小溪的优雅；没有原野的芬芳，可以有小草的翠绿！生活中没有旁观者的席位，我们总能找到自己的位置，自己的光源，自己的声音。我们有美的胸襟，我们才活得坦然；我们活得坦然，生活才给我们快乐的体验。&nbsp;</span></p>\n<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 菲尔丁说：\"不好的书也像不好的朋友一样，可能会把你戕害。\"这话没错。但不必为此走向另一极端，夸大书籍对人的品格的影响。更多的情况是：好人读了坏书仍是好人，坏人读了好书仍是坏人。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">友情，是人生一笔受益匪浅的储蓄。这储蓄，是患难中的倾囊相助，是错误道路上的逆耳忠言，是跌倒时的一把真诚搀扶，是痛苦时抹去泪水的一缕春风。</span></p>', '2017-11-20 00:00:10', 'Y', 'Y', null, '2017-11-20 00:00:09', '2017-11-20 01:00:09', '爱心是一片照射在冬日的阳光');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd50470230005', 'kinglone', '滴水之所以能够穿石，原因起码有二：一是在于它们目标专一，每一滴水都朝着同一方向，落在一个定点上；二是在于它们持之以恒，在漫长的岁月中，它们从未间断过这种努力。由此及彼，我们可以想到古今中外有成就的学者，在它们身上，都体现了\"专\"\"恒\"二字。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 滴水之所以能够穿石，原因起码有二：一是在于它们目标专一，每一滴水都朝着同一方向，落在一个定点上；二是在于它们持之以恒，在漫长的岁月中，它们从未间断过这种努力。由此及彼，我们可以想到古今中外有成就的学者，在它们身上，都体现了\"专\"\"恒\"二字。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 母子之爱、兄弟之情，虽然也有浓厚的利他色彩，但它的前提是以血缘和亲情为纽带的，还不能称得上无私奉献。只有超越亲情血缘，超越个人功利之上的利他和牺牲，才称锝上无私奉献。这种无私奉献，既不同于道德家的界说，也不同于 慈善家的善行。它不是从\"小我\"意义上寻求和感悟精神的皈依和心灵的慰藉，而是从\"大我\"的意义上承诺和践行对国家、社会和人民群众的责任和使命。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 书籍好比一架梯子，它能引导我们登上知识的殿堂。书籍如同一把钥匙，它将帮助我们开启心灵的智慧之窗。时间好比一个良医，它能教我们医治流血的伤口，时间如同一位慈母，它将帮助我们抚平心灵的创伤。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 只有波涛汹涌的大海，才能创造出沙滩的光洁与柔软，而平静的湖边，只好让污泥环绕；只有狂风暴雨，才能洗涤污泥浊水，而和风细雨，只能留下泥沙；只有一望无际的森林，才能创造出大地的翠绿与娇美，而孤独的树木，只好任狂风摧残。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;热爱是什么？热爱是风，热爱是雨。因为热爱，我们甘于淡泊宁静的日子；也因为热爱，我门敢于金戈铁马去，马革裹尸还，兴趣是什么？兴趣是热，兴趣是爱。因为兴趣，我们抛弃了兴趣以外的享乐；也因为兴趣，我们探索奥秘去，收获成果还。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;书是我的精神食粮，它 重塑了我的灵魂。当简&middot;爱说：\"我们是平等的，我不是无感情的机器\"，我懂得了作为女性的自尊；当裴多菲说：\"若为自由故，二者皆可抛\"，我懂得了作为人的价值；当鲁迅说：\"不在沉默中爆发，就在沉默中灭亡\"，我懂得了人应具有的反抗精神；当白朗宁说：\"拿走爱，世界将变成一座坟墓\"，我懂得了为他人奉献爱心是多么重要。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;既然目标是遥远的地平线，就该多一些冷静和从容。秋天的累累硕果全靠春天的辛勤播种；成为大海的一脉热血，全靠小溪日夜不停地奔涌。也许，付出了汗水，不一定能收获那个日渐成熟的梦；也许，洒下了热血，并不一定拥有那片火红的风景；也许，黑暗中的苦苦挣扎，并不一定迎来灿烂的黎明......。可我并不一味伤感。尽管默默地耕耘，还未听到生命的回声，但热爱生命的我，一定会赢。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 用不着把年轻的心灵装点得沉重。表面上的沧桑，外在的严肃，并不能让你上升为哲人；离开所有的朋友，你有的只能是孤单的背影。既然现在的我还不能变得深刻，那么，我就让自己变得轻松。哭丧着脸的人，怎能听清花开的响声；伪装自己的人，又怎能听懂蛙鸣一片里的激动。&nbsp;</span></p>', '2017-11-20 00:02:39', 'Y', 'Y', null, '2017-11-20 00:02:38', '2017-11-20 02:02:38', '滴水之所以能够穿石');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd50470230444', 'kinglone', '滴水之所以能够穿石，原因起码有二：一是在于它们目标专一，每一滴水都朝着同一方向，落在一个定点上；二是在于它们持之以恒，在漫长的岁月中，它们从未间断过这种努力。由此及彼，我们可以想到古今中外有成就的学者，在它们身上，都体现了\"专\"\"恒\"二字。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 滴水之所以能够穿石，原因起码有二：一是在于它们目标专一，每一滴水都朝着同一方向，落在一个定点上；二是在于它们持之以恒，在漫长的岁月中，它们从未间断过这种努力。由此及彼，我们可以想到古今中外有成就的学者，在它们身上，都体现了\"专\"\"恒\"二字。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 母子之爱、兄弟之情，虽然也有浓厚的利他色彩，但它的前提是以血缘和亲情为纽带的，还不能称得上无私奉献。只有超越亲情血缘，超越个人功利之上的利他和牺牲，才称锝上无私奉献。这种无私奉献，既不同于道德家的界说，也不同于 慈善家的善行。它不是从\"小我\"意义上寻求和感悟精神的皈依和心灵的慰藉，而是从\"大我\"的意义上承诺和践行对国家、社会和人民群众的责任和使命。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 书籍好比一架梯子，它能引导我们登上知识的殿堂。书籍如同一把钥匙，它将帮助我们开启心灵的智慧之窗。时间好比一个良医，它能教我们医治流血的伤口，时间如同一位慈母，它将帮助我们抚平心灵的创伤。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 只有波涛汹涌的大海，才能创造出沙滩的光洁与柔软，而平静的湖边，只好让污泥环绕；只有狂风暴雨，才能洗涤污泥浊水，而和风细雨，只能留下泥沙；只有一望无际的森林，才能创造出大地的翠绿与娇美，而孤独的树木，只好任狂风摧残。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;热爱是什么？热爱是风，热爱是雨。因为热爱，我们甘于淡泊宁静的日子；也因为热爱，我门敢于金戈铁马去，马革裹尸还，兴趣是什么？兴趣是热，兴趣是爱。因为兴趣，我们抛弃了兴趣以外的享乐；也因为兴趣，我们探索奥秘去，收获成果还。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;书是我的精神食粮，它 重塑了我的灵魂。当简&middot;爱说：\"我们是平等的，我不是无感情的机器\"，我懂得了作为女性的自尊；当裴多菲说：\"若为自由故，二者皆可抛\"，我懂得了作为人的价值；当鲁迅说：\"不在沉默中爆发，就在沉默中灭亡\"，我懂得了人应具有的反抗精神；当白朗宁说：\"拿走爱，世界将变成一座坟墓\"，我懂得了为他人奉献爱心是多么重要。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;既然目标是遥远的地平线，就该多一些冷静和从容。秋天的累累硕果全靠春天的辛勤播种；成为大海的一脉热血，全靠小溪日夜不停地奔涌。也许，付出了汗水，不一定能收获那个日渐成熟的梦；也许，洒下了热血，并不一定拥有那片火红的风景；也许，黑暗中的苦苦挣扎，并不一定迎来灿烂的黎明......。可我并不一味伤感。尽管默默地耕耘，还未听到生命的回声，但热爱生命的我，一定会赢。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 用不着把年轻的心灵装点得沉重。表面上的沧桑，外在的严肃，并不能让你上升为哲人；离开所有的朋友，你有的只能是孤单的背影。既然现在的我还不能变得深刻，那么，我就让自己变得轻松。哭丧着脸的人，怎能听清花开的响声；伪装自己的人，又怎能听懂蛙鸣一片里的激动。&nbsp;</span></p>', '2017-11-20 00:02:39', 'Y', 'Y', null, '2017-11-20 00:02:38', '2017-11-20 03:02:38', '滴水之所以能够穿石22');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd516a30e0009', 'kinglone', '人生虽然只有几十年，却决不是梦一样幻灭，只要追求真理，便得永生。人生不是燃烧的蜡烛，而是举着的火炬，我们必须把它燃烧得光明炽热，传给下一代的人。 \n\n 我是一朵白云，亲情是包容我的蓝天；我是一棵绿树，亲情便是滋养我的土地；我是一只飞鸟，亲情便是庇护我的森林；我是一泓清泉，亲情便是拥抱我的山峦。 ', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; 人生虽然只有几十年，却决不是梦一样幻灭，只要追求真理，便得永生。人生不是燃烧的蜡烛，而是举着的火炬，我们必须把它燃烧得光明炽热，传给下一代的人。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp;我是一朵白云，亲情是包容我的蓝天；我是一棵绿树，亲情便是滋养我的土地；我是一只飞鸟，亲情便是庇护我的森林；我是一泓清泉，亲情便是拥抱我的山峦。&nbsp;</span><br style=\"font-family: tahoma, ����;\" />&nbsp;<img src=\"../upload/blog/20171120/1511108522239.jpg\" width=\"368\" height=\"207\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 失败，是把有价值的东西毁灭给人看；成功，是把有价值的东西包装给人看。成功的秘诀是不怕失败和不忘失败。成功者都是从失败的炼狱中走出来的。成功与失败循环往复，构成精彩的人生。成功与失败的裁决，不是在起点，而是在终点。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 人生就像一条河，经历丰富，才能远源流长。伟大的一生，像黄河一样跌宕起伏，像长江一样神奇壮美。人生就像一座山，经历奇特才能蔚为大观。伟大的一生，像黄山一样奇峰迭起，像泰山一样大气磅礴。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">经历就是人生的硎石，生命的锋芒在磨砺中闪光；经历就是人生的矿石，生命的活力在提炼中释放。经历就是体验，经历就是积淀。没有体验就没有生存的质量；没有积淀，就没有生存的智慧。人生的真谛在经历中探寻，人生的价值在经历中实现。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 美，可以在金碧辉煌的宫殿中，也可以在炸毁的大桥旁；美，可以在芳香扑鼻的鲜花上，也可以在风中跳动的烛光中；美，可以在超凡脱俗的维那斯雕像上，也可以在平凡少女的笑靥里。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 用不朽的\"人\"字支撑起来的美好风景，既有\"虽体解吾犹未变兮\"的执着吟哦，也有\"我辈岂是蓬蒿人\"的跌宕胸怀；既有\"我以我血荐轩辕\"的崇高追求，也有\"敢教日月换新天\"的豪放气魄。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp;我是一只蜜蜂，在祖国的花园里，飞来飞去，不知疲倦地为祖国酿制甘甜的蜂蜜；我是一只紫燕，在祖国的蓝天上，穿越千家万户，向祖国向人民报告春的信息；我是一滴雨点，在祖国的原野上，从天而降，滋润干渴的禾苗；我是一株青松，在祖国的边疆，傲然屹立，显示出庄严的身姿。&nbsp;</span></p>', '2017-11-20 00:22:31', 'Y', 'Y', null, '2017-11-20 00:22:31', '2017-11-20 04:22:31', '人生虽然只有几十年');
INSERT INTO `article` VALUES ('97d883a75fd4f298015fd518efa4000b', 'kinglone', '信仰是人生杠杆的支撑点，具备这个支撑点，才可能成为一个强而有力的人；信仰是事业的大门，没有正确的信仰，注定做不出伟大的事业。劳动是一切知识的源泉，只有辛勤劳动，才能点燃智慧的熊熊大火；劳动是事业的阶梯，没有辛勤的劳动，不可能攀上事业成功的巅峰。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;信仰是人生杠杆的支撑点，具备这个支撑点，才可能成为一个强而有力的人；信仰是事业的大门，没有正确的信仰，注定做不出伟大的事业。劳动是一切知识的源泉，只有辛勤劳动，才能点燃智慧的熊熊大火；劳动是事业的阶梯，没有辛勤的劳动，不可能攀上事业成功的巅峰。&nbsp;</span>&nbsp;&nbsp;<img src=\"../upload/blog/20171120/1511108655706.jpg\" width=\"502\" height=\"401\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 如果说人生是一首优美的乐曲，那么痛苦则是其中一个不可缺少的音符；如果说人生是一望无际的大海，那么挫折则是一个骤然翻起的浪花；如果说人生是湛蓝的天空，那么失意则是一朵漂浮的白云。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 一个人要正视和克服自身的弱点，需要有十倍的勇气和百倍的坚强。有的人会因为自己聪明能干或\"血统高贵\"而骄傲自大，他要战胜这个弱点，不知要碰多少回壁，挨多少次批评，作多少番深深的反省，他们是人生征途上马拉松冠军。有的人为了战胜疾病和伤残，忍受着精神和肉体上的痛苦，无畏地向死神宣战，坚忍地同命运抗争，把厄运的千斤重压举起和推倒，令重量级的举重猛将也肃然起敬。还有那些为战胜私欲而处处克己的人，为战胜惰性而反复自策的人，为战胜暴躁而时时制怒的人，为战胜怯懦而不断自励的人。他们，都是生活的强者。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;名人和凡人差别在什么地方呢？名人用过的东西就是文物了，凡人用过的东西就是废物；名人做一点错事，写起来叫名人逸事，凡人做了错事，谈起来就说他是犯傻；名人强词夺理，叫做雄辩，凡人强词夺理就是狡辩了；名人打扮得不修边幅，叫做艺术家的气质，凡人不修边幅，就是流里流气。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp;我不是傲霜斗雪的青松，也不是委身厅堂的盆景，而是广袤无垠的土地上的一株小草--点缀风景如画的大自然。我不是璀璨夺目的明珠，也不是一过即逝的星辰，而是茫茫大海中的一排浪花--装扮勇敢无畏的探索者。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp;春天像一个步履轻盈的小姑娘。她携着神奇的花篮，把五彩的鲜花撒向山坡，撒向原野。她伴着欢快的溪流，把婉转的歌儿唱给青山，唱给牧童。秋天像一位技艺高超的丹青手，她端起巨大的画板，把金黄的水粉洒向田野，洒向村庄。她举起神奇的画笔，，用斑斓的色彩点染树梢，点染山峦。&nbsp;</span></p>', '2017-11-20 00:25:02', 'Y', 'Y', null, '2017-11-20 00:25:02', '2017-11-20 05:25:02', '信仰是人生杠杆的支撑点');
INSERT INTO `article` VALUES ('97d883a75fd51eaf015fd52117810000', 'kinglone', '光阴是一把神奇而无情的雕刻刀，在天地之间创造着种种奇迹。它能把巨石分裂成尘土，把幼苗雕成大树，把荒漠变成城市和园林；当然，它也能使繁华之都衰败成荒原与废墟，使锃亮的金属爬满绿锈，失去光泽。老人额头上的皱纹是它刻出来的，少女脸上的红润是它描绘出来的。生命的繁衍与世界的运动正是由它精心指挥着。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 光阴是一把神奇而无情的雕刻刀，在天地之间创造着种种奇迹。它能把巨石分裂成尘土，把幼苗雕成大树，把荒漠变成城市和园林；当然，它也能使繁华之都衰败成荒原与废墟，使锃亮的金属爬满绿锈，失去光泽。老人额头上的皱纹是它刻出来的，少女脸上的红润是它描绘出来的。生命的繁衍与世界的运动正是由它精心指挥着。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><img src=\"../upload/blog/20171120/1511109219606.jpg\" width=\"421\" height=\"664\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 春天，不单是四季之首的名词，春天，也与美好在一起。挨过漫漫严冬，人们希望春光永驻；听着谆谆教诲，人们感觉如坐春风。春晖，为诗歌增添亮色；春雨，使图画洋溢生机。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 人生如一支歌，应该多一些昂扬的旋律，少一些萎靡的音符；人生如一首诗，应该多一些热烈的抒情，少一些愁苦的叹息；人生如一幅画，应该多一些亮丽的色彩，少一些灰暗的色调；人生如一棵树。应该多一些新鲜的翠绿，少一些凋零的枯萎。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 希望大路平坦笔直，却常常有岔路和崎岖；希望江河一泻千里，却常常有旋涡与逆流；希望庄稼茂盛丰收，却常常有旱劳与虫害；希望人生幸福美满，却常常有挫折和失败。事物总是由对立的两方面组成的，希望万事如意是不现实的。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 生命原来是梦想的一架梯子，可以延伸到梦想成真的那一刻，只要你永不放弃。挫折原来是成功的一块基石，可以垒出希望的丰碑，只要你决不退缩；真诚原来是沟通的一把钥匙，可以开启人们封闭的心扉，只要你坚持到底。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 如果说，眼睛是心灵洞开的一对窗扇，是心灵涌出的两汪清泉；那么，秘密，就是心灵珍藏的一座宝矿，是心灵敛聚的一抹灵光。是不是，心灵中有了一个秘密，才称得上是一个真正的人？&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp;如果说，读书是在奠定人生的基石，在梳理人生的羽毛，那么，实践，就是在构建人生的厅堂，历练人生的翅膀。是不是，人生经过了实践，才能真正矗立、飞翔在天地之间？&nbsp;</span></p>', '2017-11-20 00:33:57', 'Y', 'Y', null, '2017-11-20 00:33:56', '2017-11-20 06:33:56', '光阴是一把神奇而无情的雕刻刀');
INSERT INTO `article` VALUES ('97d883a75fd51eaf015fd52117810011', 'kinglone', '光阴是一把神奇而无情的雕刻刀，在天地之间创造着种种奇迹。它能把巨石分裂成尘土，把幼苗雕成大树，把荒漠变成城市和园林；当然，它也能使繁华之都衰败成荒原与废墟，使锃亮的金属爬满绿锈，失去光泽。老人额头上的皱纹是它刻出来的，少女脸上的红润是它描绘出来的。生命的繁衍与世界的运动正是由它精心指挥着。', '4028b8815fced709015fcedfe2b70001', '<p><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; &nbsp; 光阴是一把神奇而无情的雕刻刀，在天地之间创造着种种奇迹。它能把巨石分裂成尘土，把幼苗雕成大树，把荒漠变成城市和园林；当然，它也能使繁华之都衰败成荒原与废墟，使锃亮的金属爬满绿锈，失去光泽。老人额头上的皱纹是它刻出来的，少女脸上的红润是它描绘出来的。生命的繁衍与世界的运动正是由它精心指挥着。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><img src=\"../upload/blog/20171120/1511109219606.jpg\" width=\"421\" height=\"664\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 春天，不单是四季之首的名词，春天，也与美好在一起。挨过漫漫严冬，人们希望春光永驻；听着谆谆教诲，人们感觉如坐春风。春晖，为诗歌增添亮色；春雨，使图画洋溢生机。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 人生如一支歌，应该多一些昂扬的旋律，少一些萎靡的音符；人生如一首诗，应该多一些热烈的抒情，少一些愁苦的叹息；人生如一幅画，应该多一些亮丽的色彩，少一些灰暗的色调；人生如一棵树。应该多一些新鲜的翠绿，少一些凋零的枯萎。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 希望大路平坦笔直，却常常有岔路和崎岖；希望江河一泻千里，却常常有旋涡与逆流；希望庄稼茂盛丰收，却常常有旱劳与虫害；希望人生幸福美满，却常常有挫折和失败。事物总是由对立的两方面组成的，希望万事如意是不现实的。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 生命原来是梦想的一架梯子，可以延伸到梦想成真的那一刻，只要你永不放弃。挫折原来是成功的一块基石，可以垒出希望的丰碑，只要你决不退缩；真诚原来是沟通的一把钥匙，可以开启人们封闭的心扉，只要你坚持到底。&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp; 如果说，眼睛是心灵洞开的一对窗扇，是心灵涌出的两汪清泉；那么，秘密，就是心灵珍藏的一座宝矿，是心灵敛聚的一抹灵光。是不是，心灵中有了一个秘密，才称得上是一个真正的人？&nbsp;</span><br style=\"font-family: tahoma, ����;\" /><br style=\"font-family: tahoma, ����;\" /><span style=\"font-family: tahoma, ����;\">&nbsp; &nbsp; &nbsp;如果说，读书是在奠定人生的基石，在梳理人生的羽毛，那么，实践，就是在构建人生的厅堂，历练人生的翅膀。是不是，人生经过了实践，才能真正矗立、飞翔在天地之间？&nbsp;</span></p>', '2017-11-20 00:33:57', 'N', 'Y', null, '2017-11-20 00:33:56', '2017-11-20 07:33:56', '光阴是一把神奇而无情的雕刻刀2222');
INSERT INTO `article` VALUES ('97d883a75fd51eaf015fd5294dfb0002', 'kinglone', '图片上传部分代码', '4028b8815fcf346d015fcf35bf4f0001', '<p>下面是图片上传部分代码：</p>\n<pre class=\"language-java\"><code>	@RequestMapping(\"/image/fileUpload.jspx\")\n	public JSONMessageView fileUpload(HttpServletRequest request, Model model) {\n		JSONMessageView json = new JSONMessageView(-1, \"上传失败!\", null);\n		try {\n			Map&lt;String, String&gt; map = FileUtils.saveByRequest(request, \"blog\", 1024 * 1024, null, null);\n			for (String key : map.keySet()) {\n				if (map.get(key).equals(\"error\")) {\n					json.setCode(-1);\n					json.setMessage(\"上传图片太大\");\n				} else {\n					json.setCode(0);\n					json.setMessage(\"上传成功!\");\n					json.setContent(map.get(key));\n					System.out.println(json);\n				}\n				break;\n			}\n			return json;\n		} catch (Exception e) {\n			e.printStackTrace();\n			json.setCode(-1);\n			json.setMessage(\"服务器异常,请稍候再试!\");\n			return json;\n		}\n\n	}</code></pre>', '2017-11-20 00:42:55', 'Y', 'N', null, '2017-11-20 00:42:54', '2017-11-20 08:42:54', '图片上传部分代码22');

-- ----------------------------
-- Table structure for classify
-- ----------------------------
DROP TABLE IF EXISTS `classify`;
CREATE TABLE `classify` (
  `sysid` varchar(32) NOT NULL,
  `classifyName` varchar(32) NOT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classify
-- ----------------------------
INSERT INTO `classify` VALUES ('4028b8815fced709015fcedfe2b70001', '作文', '2017-11-18 19:25:00');
INSERT INTO `classify` VALUES ('4028b8815fcf346d015fcf35bf4f0001', '文章', '2017-11-18 20:58:47');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `sysid` varchar(32) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `createtime` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `isDisplay` varchar(1) NOT NULL,
  `nickname` varchar(32) NOT NULL,
  `pid` varchar(32) DEFAULT NULL,
  `websiteUrl` varchar(256) DEFAULT NULL,
  `articleId` varchar(32) NOT NULL,
  PRIMARY KEY (`sysid`),
  KEY `FK_8cbe7hutud62orb5whgikvbxa` (`articleId`),
  CONSTRAINT `FK_8cbe7hutud62orb5whgikvbxa` FOREIGN KEY (`articleId`) REFERENCES `article` (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('40289f865ff1507f015ff155aa030002', 'dsds', '2017-11-25 12:00:44', 'kk@qq.com', 'Y', 'kkk', null, '', '40289f865ff1507f015ff15426970001');
INSERT INTO `comment` VALUES ('97d883a75fd51eaf015fd52a5f6b0003', '不错不错，好文好文', '2017-11-20 00:44:04', 'shijie@qq.com', 'Y', '世界那么大，我想去看看', null, '', '97d883a75fd51eaf015fd52117810000');
INSERT INTO `comment` VALUES ('97d883a75fd51eaf015fd5594b540004', '棒！', '2017-11-20 01:35:19', 'dr@qq.com', 'Y', '淡然', null, '', '97d883a75fd51eaf015fd5294dfb0002');

-- ----------------------------
-- Table structure for friendlinks
-- ----------------------------
DROP TABLE IF EXISTS `friendlinks`;
CREATE TABLE `friendlinks` (
  `sysid` varchar(32) NOT NULL,
  `createtime` varchar(20) NOT NULL,
  `isDisplay` varchar(1) NOT NULL,
  `nickname` varchar(128) NOT NULL,
  `weburl` varchar(128) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friendlinks
-- ----------------------------
INSERT INTO `friendlinks` VALUES ('4028b8815fb5fb39015fb602d8340002', '2017-11-13 23:32:40', 'Y', '百度', 'https://www.baidu.com', '123123123', '奥术大师大所大所多');

-- ----------------------------
-- Table structure for loginlog
-- ----------------------------
DROP TABLE IF EXISTS `loginlog`;
CREATE TABLE `loginlog` (
  `sysid` varchar(32) NOT NULL,
  `clientIP` varchar(20) DEFAULT NULL,
  `loginTime` varchar(20) NOT NULL,
  `remoteIP` varchar(20) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of loginlog
-- ----------------------------
INSERT INTO `loginlog` VALUES ('402881ab5fdd44c6015fdd5ec6850006', null, '2017-11-21 14:58:16', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdd7dd8015fdd7e3b1a0000', null, '2017-11-21 15:32:38', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdd7dd8015fdd7eb6090001', null, '2017-11-21 15:33:09', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdd89b1015fdd8a14fb0000', null, '2017-11-21 15:45:34', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdd8e42015fdd8f4a9f0000', null, '2017-11-21 15:51:16', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdd9e50015fdd9f169a0000', null, '2017-11-21 16:08:31', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdda013015fdda078700000', null, '2017-11-21 16:10:02', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdda69a015fdda727c10000', null, '2017-11-21 16:17:20', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fdda83c015fdda8aec00000', null, '2017-11-21 16:19:00', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('402881ab5fddaa0e015fddaa7f9e0000', null, '2017-11-21 16:20:59', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865fdec829015fdeec45950000', null, '2017-11-21 22:12:27', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff10284015ff1032c620000', null, '2017-11-25 10:30:37', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff10645015ff106e8c80000', null, '2017-11-25 10:34:42', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff1107f015ff11101fa0000', null, '2017-11-25 10:45:44', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff11514015ff115c0d10000', null, '2017-11-25 10:50:55', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff1442c015ff14542af0000', null, '2017-11-25 11:42:48', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff1507f015ff1533d9e0000', null, '2017-11-25 11:58:05', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('40289f865ff196cd015ff197f0b90000', null, '2017-11-25 13:13:07', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f869e71015f869eb4b10000', null, '2017-11-04 18:41:08', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f86a098015f86a0ecc40000', null, '2017-11-04 18:43:34', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f8a1cad015f8a1d313f0000', null, '2017-11-05 10:58:09', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f96a9cb015f96aa82870000', null, '2017-11-07 21:27:57', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f96a9cb015f9715dfac0001', null, '2017-11-07 23:25:14', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f9c2f80015f9c3093270000', null, '2017-11-08 23:12:29', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f9c364a015f9c68dac70000', null, '2017-11-09 00:13:58', '0:0:0:0:0:0:0:1', 'success', 'admin');
INSERT INTO `loginlog` VALUES ('4028b8815f9c7b74015f9c8c80120000', null, '2017-11-09 00:52:54', '0:0:0:0:0:0:0:1', 'success', 'admin');

-- ----------------------------
-- Table structure for music
-- ----------------------------
DROP TABLE IF EXISTS `music`;
CREATE TABLE `music` (
  `createtime` varchar(20) NOT NULL,
  `enabled` varchar(20) NOT NULL,
  `musicName` varchar(20) NOT NULL,
  `musicUrl` varchar(128) NOT NULL,
  `sequence` varchar(20) NOT NULL,
  `sysid` varchar(32) NOT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of music
-- ----------------------------
INSERT INTO `music` VALUES ('2017-11-20 15:28:32', 'Y', '眉间雪', '/upload/blog/20171120/1511162896758.mp3', '1', '402881ab5fd8470e015fd8541dac0005');
INSERT INTO `music` VALUES ('2017-11-20 15:29:47', 'Y', '佛秀', '/upload/blog/20171120/1511162978462.mp3', '2', '402881ab5fd8470e015fd85544e20006');

-- ----------------------------
-- Table structure for systemparam
-- ----------------------------
DROP TABLE IF EXISTS `systemparam`;
CREATE TABLE `systemparam` (
  `sysid` varchar(32) NOT NULL,
  `operationTime` varchar(19) NOT NULL,
  `paramDateType` varchar(255) NOT NULL,
  `paramName` varchar(255) NOT NULL,
  `paramValue` varchar(255) NOT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemparam
-- ----------------------------
INSERT INTO `systemparam` VALUES ('author', '2017-11-18 19:30:32', 'String', '首页页眉作者', 'kinglone');
INSERT INTO `systemparam` VALUES ('copyright', '2017-11-18 19:30:32', 'String', '首页页眉页脚copyright', 'www.kinglone.com');
INSERT INTO `systemparam` VALUES ('leftFooter', '2017-11-18 19:30:32', 'String', '后台左下角', '智能办公平台 v1.011');
INSERT INTO `systemparam` VALUES ('logion', '2017-11-18 19:30:32', 'String', '名言', '青春无非是一场梦,得拼得闯得奋斗!');
INSERT INTO `systemparam` VALUES ('rightFooter', '2017-11-18 19:30:32', 'String', '后台右下角copyright', 'Copyright © 2016 - 2017 kinglone-分享知识，传递希望 版权所有');
INSERT INTO `systemparam` VALUES ('statement', '2017-11-18 19:30:32', 'String', '原创声明', '这是我个人对写博客的一点心得，每个写博客的人都有自己的心得，我这里列举出来的肯定不全，也有些是你并不认同的。但是，我相信还是有部分是正确有用的.');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `sysid` varchar(32) NOT NULL,
  `email` varchar(20) NOT NULL,
  `likename` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('343243242342342342342342', '1015817394@qq.com', 'kinglone', '0a72518c46ba7e1d3cc47c69ba33bc32', '13268351003', 'M', 'admin');
