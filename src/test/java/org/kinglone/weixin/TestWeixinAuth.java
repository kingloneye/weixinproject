package org.kinglone.weixin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.junit.Test;
import org.kinglone.weixin.kit.WeixinBasicKit;
import org.kinglone.weixin.model.WeixinFinalValue;
import org.kinglone.weixin.web.servlet.WeixinContext;

public class TestWeixinAuth {

	@Test
	public void testAuth() throws UnsupportedEncodingException {
		String url = WeixinFinalValue.AUTH_URL;
		url = url.replaceAll("APPID", WeixinContext.getInstance().getAppId())
				 .replace("REDIRECT_URI", URLEncoder.encode("http://www.konghao.org","utf-8"))
				 .replace("STATE", "java123");
		String con = WeixinBasicKit.sendGet(url);
		System.out.println(con);
	}
}
