package org.kinglone.basic.utils.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.kinglone.basic.utils.PageQuery;
import org.kinglone.basic.utils.PageResults;
import org.kinglone.basic.utils.PageResultsObject;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;



public interface BaseDao<T>{
	/**
     * <保存实体>
     * <完整保存实体>
     * @param t 实体参数
     */
	@Transactional
    public abstract void save(T t);
	
	/**
     * <保存实体>
     * <完整保存实体>
     * @param entity 实体参数
     */
	@Transactional
    public abstract void save(String entityName, Object entity);
 
    /**
     * <保存或者更新实体>
     * @param t 实体
     */
	@Transactional
    public abstract void saveOrUpdate(T t);
 
    /**
     * <load>
     * <加载实体的load方法>
     * @param id 实体的id
     * @return 查询出来的实体
     */
    public abstract T load(Serializable id);
 
    /**
     * <get>
     * <查找的get方法>
     * @param id 实体的id
     * @return 查询出来的实体
     */
    @Transactional
    public abstract T get(Serializable id);
 
    /**
     * <contains>
     * @param t 实体
     * @return 是否包含
     */
    public abstract boolean contains(T t);
 
    /**
     * <delete>
     * <删除表中的t数据>
     * @param t 实体
     */
    @Transactional
    public abstract void delete(T t);
 
    /**
     * <根据ID删除数据>
     * @param Id 实体id
     * @return 是否删除成功
     */
    @Transactional
    public abstract boolean deleteById(Serializable Id);
 
    /**
     * <删除所有>
     * @param entities 实体的Collection集合
     */
    @Transactional
    public abstract void deleteAll(Collection<T> entities);
     
    /**
     * <执行Hql语句>
     * @param hqlString hql
     * @param values 不定参数数组
     */
    public abstract void queryHql(String hqlString, Object... values); 
     
    /**
     * <执行Sql语句>
     * @param sqlString sql
     * @param values 不定参数数组
     */
    public abstract void querySql(String sqlString, Object... values); 
 
    /**
     * <根据HQL语句查找唯一实体>
     * @param hqlString HQL语句
     * @param values 不定参数的Object数组
     * @return 查询实体
     */
    public abstract T getByHQL(String hqlString, Object... values);
 
    /**
     * <根据SQL语句查找唯一实体>
     * @param sqlString SQL语句
     * @param values 不定参数的Object数组
     * @return 查询实体
     */
    public abstract T getBySQL(String sqlString, Object... values);
 
    /**
     * <根据HQL语句，得到对应的list>
     * @param hqlString HQL语句
     * @param values 不定参数的Object数组
     * @return 查询多个实体的List集合
     */
    public abstract List<T> getListByHQL(String hqlString, Object... values);
 
    /**
     * <根据SQL语句，得到对应的list>
     * @param sqlString HQL语句
     * @param values 不定参数的Object数组
     * @return 查询多个实体的List集合
     */
    public abstract List<T> getListBySQL(String sqlString, Object... values);
     
    /**
     * 由sql语句得到List
     * @param sql
     * @param map
     * @param values
     * @return List
     */
    public List findListBySql(final String sql, final RowMapper map, final Object... values);
 
    /**
     * <refresh>
     * @param t 实体
     */
    @Transactional
    public abstract void refresh(T t);
 
    /**
     * <update>
     * @param t 实体
     */
    @Transactional
    public abstract void update(T t);
    /**
     * 执行hql更新
     * @param hql	
     * @param values
     * @return
     */
    @Transactional
    public int executeUpdateHql(String hql,Object... values);
    /**
     * 执行sql更新
     * @param sql
     * @param values
     * @return
     */
    @Transactional
    public int executeUpdateSql(String sql,Object... values);
    
    /**
     * <根据HQL得到记录数>
     * @param hql HQL语句
     * @param values 不定参数的Object数组
     * @return 记录总数
     */
    public abstract Long countByHql(String hql, Map<String, Object> values);
    
    /**
     * 
      * <HQL分页查询>
     * @param hql HQL语句
     * @param countHql 查询记录条数的HQL语句
     * @param pageNo 下一页
     * @param pageSize 一页总条数
     * @param values 不定Object数组参数
     * @return PageResults的封装类，里面包含了页码的信息以及查询的数据List集合
     */
    public abstract PageResults<T> findPageByFetchedHql(String hql, String countHql, int pageNo, int pageSize, Map<String,Object> values);
    
    /**
     * <HQL分页查询>
     * @param hql			hql HQL语句
     * @param countHql		查询记录条数的HQL语句
     * @param pageQuery		分页参数
     * @param values		不定Object数组参数
     * @return
     */
    public abstract PageResults<T> findPageByFetchedHql(String hql, String countHql, PageQuery pageQuery, Map<String, Object> values);
  
    
    /**
     * 
      * <HQL分页查询>
     * @param hql HQL语句
     * @param countSql 查询记录条数的SQL语句
     * @param pageNo 下一页
     * @param pageSize 一页总条数
     * @param values 不定Object数组参数
     * @param sqlvalues  不定sql Object数组参数
     * @return PageResults的封装类，里面包含了页码的信息以及查询的数据List集合
     */
    public abstract PageResults<T> findPageByHqlAndCountSql(String hql, String countHql, int pageNo, int pageSize, Map<String,Object> values, Map<String,Object> sqlvalues);

    /** 
    * 描述:HQL查询，SQL统计总数
    * 
    * @param hql  hql语句
    * @param countSql  查询记录SQl语句
    * @param pageQuery   分页参数
    * @param values  不定Object数组参数
    * @param sqlvalues  不定sql Object数组参数
    * @return 
    * PageResults<T>
    *
    * @author  HuShiJin 
    * @date 创建时间：2017年5月5日 上午8:50:25  
    * 
    */
    public abstract PageResults<T> findPageByHqlAndCountSql(String hql, String countSql, PageQuery pageQuery, Map<String, Object> values, Map<String,Object> sqlvalues);
    
    public abstract PageResultsObject findPageByFetchedSql(String sql, String countSql, int pageNo, int pageSize, Map<String,Object> values);
    
    public abstract PageResultsObject findPageByFetchedSql(String sql, String countSql, PageQuery pageQuery, Map<String, Object> values);
  
    public abstract List<T> getAll();
    
    public abstract List<T> getAll(Class<T> entityClass);
    
    /**
   	 * 多条件分页查询
   	 * @param hql query string
   	 * @param startRow begin row
   	 * @param pageSize page number
   	 * @param params query object params array
   	 * @return the query list<?> result
   	 * @see org.hibernate#Session
        * @throws java.lang.IllegalArgumentException if queryString is null
   	 */
       public <T> List<T> findByPage(String queryString, Integer startRow,
   	    Integer pageSize, Object... params);
    
	
	
	   /**
     * 统计总条数
     * @param hql
     * @return
     */
    public Integer count(String hql);
    
    /**
     * 根据条件统计总条数
     * @param hql
     * @param obj
     * @return
     */
    public Integer count(String hql, Object ... obj);
}
