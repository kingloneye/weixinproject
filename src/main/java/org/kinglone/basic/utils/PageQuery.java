package org.kinglone.basic.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * 页面查询参数封装
 * Description: 
 * Author: 
 * Version: 1.0
 * Create Date Time: 2017年4月18日下午3:52:17
 * Update Date Time: 
 * @see
 */
public class PageQuery {
	/**
	 * 开始行数
	 */
	private Integer start;
	/**
	 * 每页长度
	 */
	private Integer length;
	/**
	 * 第几次请求
	 */
	private Integer draw;
	/**
	 * 排序字段
	 */
	private String customOrderStr;
	
	/**
	 * 查询信息
	 */
	private Map<Search, String> search = new HashMap<Search, String>();

	/**
	 * 排序信息
	 */
	private List<Map<Order, String>> order = new ArrayList<Map<Order, String>>();

	/**
	 * 列信息
	 */
	private List<Map<Column, String>> columns = new ArrayList<Map<Column, String>>();

	/**
	 * 查询信息
	 */
	private Map<String, String> param = new HashMap<String, String>();
	

	/**
	 * ajax 请求json数据  ：
	 * 	http://datatables.club/manual/server-side.html
	 * 
	  	draw:     2
		columns[0][data]:     0
		columns[0][name]:     
		columns[0][searchable]:     true
		columns[0][orderable]:     false
		columns[0][search][value]:     
		columns[0][search][regex]:     false
		columns[1][data]:     1
		columns[1][name]:     
		columns[1][searchable]:     true
		columns[1][orderable]:     true
		columns[1][search][value]:     
		columns[1][search][regex]:     false
		columns[2][data]:     2
		columns[2][name]:     
		columns[2][searchable]:     true
		columns[2][orderable]:     true
		columns[2][search][value]:     
		columns[2][search][regex]:     false
		order[0][column]:     1
		order[0][dir]:     desc
		start:     0
		length:     25
		search[value]:     
		search[regex]:     false
		_:     1492566273782
		
		
	 * 
	 */
	
	public PageQuery() {


	}

	


	public Map<String, String> getParam() {
		return param;
	}




	public void setParam(Map<String, String> param) {
		this.param = param;
	}




	public List<Map<Column, String>> getColumns() {
		return columns;
	}


	public void setColumns(List<Map<Column, String>> columns) {
		this.columns = columns;
	}

	public enum Search {
	value, regex
	}


	public enum Order {
	column, dir
	}


	public enum Column {
	data, name, searchable, orderable, searchValue, searchRegex
	}


	public Integer getDraw() {
	return draw;
	}


	public void setDraw(Integer draw) {
	this.draw = draw;
	}


	public Integer getStart() {
	return start;
	}


	public void setStart(Integer start) {
	this.start = start;
	}


	public Integer getLength() {
	return length;
	}


	public void setLength(Integer length) {
	this.length = length;
	}


	public Map<Search, String> getSearch() {
	return search;
	}


	public void setSearch(Map<Search, String> search) {
	this.search = search;
	}


	public List<Map<Order, String>> getOrder() {
	return order;
	}


	public void setOrder(List<Map<Order, String>> order) {
	this.order = order;
	}


	
	/**
	 * 当前页
	 * @return
	 */
	public int getCurrentPage() {
		return start / length +1 ;
	}
	/**
	 * 每页大小
	 * @return
	 */
	public int getPageSize() {
		return length  ;
	}
	
	public String getParam(String key){
		if(this.getParam() != null){
			return this.getParam().get(key);
		}
		return null;
	}
	
	/**
	 * 获取排序字符串 包括 order by 
	 * @return
	 */
	public String getOrderByStr(){
		return " ORDER BY " + this.getOrderStr();
	}
	
	/**
	 * 获取排序字符串
	 * @return
	 */
	public String getOrderStr(){
		//判断是否有自定义的排序参数
		if(!StringUtils.isEmpty(this.getCustomOrderStr())){
			return this.getCustomOrderStr();
		}
		String orderStr = "";
		//排序顺序  aes  desc
		String dir = this.getOrder().get(0).get(Order.dir);	
		Map<Column, String> columnMap = this.getColumns().get(this.getOrderColumn());
		//字段的data 设置为排序名
		orderStr = columnMap.get(Column.data).replaceAll("@_", ".") + " " + dir;
		return orderStr;
	}
	
	/**
	 * 获取排序列 从0开始
	 * @return
	 */
	public int getOrderColumn(){
		String columnStr = this.getOrder().get(0).get(Order.column);
		int column = 0;
		if(columnStr != null){
			column = Integer.valueOf(columnStr);
		}
		return column;
	}
	/**
	 * 添加查询参数
	 * @param key
	 * @param value
	 */
	public void addParam(String key,String value){
		this.getParam().put(key, value);
	}




	public String getCustomOrderStr() {
		return customOrderStr;
	}




	public void setCustomOrderStr(String customOrderStr) {
		this.customOrderStr = customOrderStr;
	}
	
	
}
