package org.kinglone.basic.utils;
import java.util.UUID;

/**
 *生成随机字符串的工具类
 * @author yejinlong
 * 2017年5月26日
 * @version 1.0
 */
public class UUIDUtils {
				
	/**
	 * 获取uuid的字符串，因为字符串有"-"，所以要截取，是32位
	 * @return
	 */
	public static String  getUUID(){					
		//System.out.println( UUID.randomUUID().toString());   					//67f941e2-906d-4d53-92ff-11e5df20cc0d
		return UUID.randomUUID().toString().replace("-", "");						//1e4c08df-64a6-4fcd-9ec4-7db62623a20e
	}
}
