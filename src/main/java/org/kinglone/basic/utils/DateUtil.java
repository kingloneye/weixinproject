package org.kinglone.basic.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * 日期转换类
 * @author zz
 *
 */
public class DateUtil {
	public static DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * String -> Date
	 */
	public static Date stringToDate(String dateStr){
		Date date = new Date();
        try {  
            date = sdf.parse(dateStr);  
        } catch (Exception e) {  
            e.printStackTrace();
        }
		return date;
	}
	/**
	 * Date -> String
	 */
	public static String dateToString(Date date){
		String dateStr = sdf.format(date);
		return dateStr;
	}
	/**
	 * Date -> String
	 */
	public static String dateToString(Date date,String format){
		if(date==null){
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(format);
		String dateStr = dateFormat.format(date);
		return dateStr;
	}
	/**
	 * String ->Timestamp
	 */
	public static Timestamp stringToTimestamp(String tsStr){
		//Timestamp ts = new Timestamp(System.currentTimeMillis());
		Timestamp ts = null;
		try {  
            ts = Timestamp.valueOf(tsStr);    
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return ts;
	}
	/**
	 * Timestamp -> String
	 */
	public static String timestampToString(Timestamp ts){	
		String tsStr = sdf.format(ts);
		return tsStr;
	}
	/**
	 * Timestamp -> String
	 */
	public static String timestampToString(Timestamp ts,String format){
		DateFormat dateFormat = new SimpleDateFormat(format);
		String tsStr = dateFormat.format(ts);
		return tsStr;
	}
	/**
	 * Timestamp -> Date Date和Timesta是父子类关系可以直接使用,不用调用此方法
	 */
	public static Date timestampToDate(Timestamp ts){
		 Date date = new Date();  
		 date = ts;
		 return date;  
	}
	/**
	 * Date -> Timestamp
	 */
	public static Timestamp dateToTimestamp(Date date){
		Timestamp ts = new Timestamp(date.getTime());
//		String dateStr = sdf.format(date);
//		Timestamp ts = new Timestamp(System.currentTimeMillis());
//		try {  
//            ts = Timestamp.valueOf(dateStr);  
//            System.out.println(ts);  
//        } catch (Exception e) {  
//            e.printStackTrace();  
//        }
		return ts;	
	}
	/**
	 * 取当天时间，整取到天如 2016-1-1 00:00:00
	 */
	public static Timestamp getDay(){
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		return getDay(ts);
	}
	
	/**
	 * 取当天时间，整取到天如 2016-1-1 00:00:00
	 */
	public static Timestamp getDay(Timestamp ts){
		Timestamp ts00 = stringToTimestamp(timestampToString(ts,"yyyy-MM-dd 00:00:00"));
		return ts00;
	}
	
	/**
	 *获取传入日期本月第一天
	 * @param args
	 */
	public static Timestamp getThisMonthFirstDay(Timestamp timestamp){
        Calendar ca = Calendar.getInstance();
        ca.setTime(timestamp);
        ca.set(Calendar.DAY_OF_MONTH, 1);  
		return new Timestamp(ca.getTimeInMillis());

	}
	/**
	 *获取传入日期本月最后一天
	 * @param args
	 */
	public static Timestamp getThisMonthLastDay(Timestamp timestamp){
        Calendar c = Calendar.getInstance(); 
        c.setTime(timestamp);
        c.set(Calendar.DAY_OF_MONTH,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
		return new Timestamp(c.getTimeInMillis());

	}
	
	/**
	 *获取上个月最后一天
	 * @param args
	 */
	public static Timestamp getLastMonthLastDay(){
        Calendar c = Calendar.getInstance();    
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
        long day = 24 * 3600 * 1000;
		return new Timestamp(c.getTimeInMillis()/ day * day-8 * 3600 * 1000);

	}
	
	/**
	 *获取传入日期上个月最后一天
	 * @param args
	 */
	public static Timestamp getLastMonthLastDay(Timestamp timestamp){
        Calendar c = Calendar.getInstance(); 
        c.setTime(timestamp);
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH,c.getActualMaximum(Calendar.DAY_OF_MONTH)); 
		return new Timestamp(c.getTimeInMillis());

	}	
	/**
	 *获取下个月第一天
	 * @param args
	 */
	public static Timestamp getNextMonthFirstDay(){
        Calendar ca = Calendar.getInstance(); 
        ca.add(Calendar.MONTH, 1);
        ca.set(Calendar.DAY_OF_MONTH, 1);  
        long day = 24 * 3600 * 1000;
		return new Timestamp(ca.getTimeInMillis()/ day * day-8 * 3600 * 1000);

	}
	/**
	 *获取传入日期下个月第一天
	 * @param args
	 */
	public static Timestamp getNextMonthFirstDay(Timestamp timestamp){
        Calendar ca = Calendar.getInstance();
        ca.setTime(timestamp);
        ca.add(Calendar.MONTH, 1);
        ca.set(Calendar.DAY_OF_MONTH, 1);  
		return new Timestamp(ca.getTimeInMillis());

	}
	
	
	public static Integer getWeek(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return (cal.get(Calendar.DAY_OF_WEEK)+6)%7;
	}
	
	
	public static void main(String[] args) {
		//TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date da = new Date();
//		long day = 24 * 3600 * 1000;
//		da.setTime(da.getTime() / day * day);//这个相当于取整，去掉小时，分钟和秒了啊
//		System.out.println(sdf.format(da));
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		System.out.println(DateUtil.getDay(ts));

	}
}
