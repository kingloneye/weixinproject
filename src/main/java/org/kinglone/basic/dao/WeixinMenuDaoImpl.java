package org.kinglone.basic.dao;

import java.util.List;

import org.kinglone.basic.model.WeixinMenu;
import org.kinglone.basic.utils.dao.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("weixinMenuDao")
public class WeixinMenuDaoImpl extends BaseDaoImpl<WeixinMenu> implements WeixinMenuDao{

	@Override
	public WeixinMenu loadByKey(String key) {
		
		return super.getByHQL("from WeixinMenu where menuKey=?", key);
	}

	@Override
	public List<WeixinMenu> listAll() {
		
		return super.getAll();
	}


}
