package org.kinglone.basic.dao;

import java.util.List;

import org.kinglone.basic.model.WeixinMenu;
import org.kinglone.basic.utils.dao.BaseDao;

public interface WeixinMenuDao extends BaseDao<WeixinMenu>{
	
	public WeixinMenu loadByKey(String key);
	
	public List<WeixinMenu> listAll();


}
