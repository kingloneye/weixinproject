package org.kinglone.basic.dao;

import java.util.List;

import org.kinglone.basic.model.User;
import org.kinglone.basic.utils.dao.BaseDao;

public interface UserDao extends BaseDao<User>{

	public User loadByUsername(String username);
	public User loadByOpenId(String openid);
	public List<User> list();
}
