package org.kinglone.basic.dao;

import java.util.List;

import org.kinglone.basic.model.User;
import org.kinglone.basic.utils.dao.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	@Override
	public User loadByUsername(String username) {
		String hql = "from User where username=?";
		return this.getByHQL(hql, username);
	}

	@Override
	public User loadByOpenId(String openid) {
		return this.getByHQL("from User where username=?", openid);
	}

	@Override
	public List<User> list() {
		return this.getAll();
	}
	
}
