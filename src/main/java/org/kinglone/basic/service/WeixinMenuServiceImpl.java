package org.kinglone.basic.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.kinglone.basic.dao.WeixinMenuDao;
import org.kinglone.basic.model.WeixinMenu;
import org.kinglone.weixin.model.WeixinMenuDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("weixinMenuService")
@Transactional(readOnly=false)
public class WeixinMenuServiceImpl implements WeixinMenuService {
	
	@Resource
	private WeixinMenuDao weixinMenuDao;

	@Override
	public WeixinMenu loadByKey(String key) {
		
		return weixinMenuDao.loadByKey(key);
		
	}

	@Override
	public void add(WeixinMenu wm) {
		if(wm.getType().equals("click")){
			wm.setMenuKey("KEY_"+System.currentTimeMillis());	
		}	
		weixinMenuDao.save(wm);	
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(WeixinMenu wm) {
		weixinMenuDao.update(wm);		
	}

	@Override
	public WeixinMenu load(int id) {

		return weixinMenuDao.load(id);
	}

	@Override
	public List<WeixinMenu> listAll() {		
		return weixinMenuDao.listAll();
	}

	@Override
	public List<WeixinMenuDto> generateWeixinMenuDto() {
		List<WeixinMenu> menus = this.listAll();
		List<WeixinMenuDto> menuDtos = new ArrayList<WeixinMenuDto>();
		WeixinMenuDto wmd = null;
		for(WeixinMenu wm:menus) {
			wmd = new WeixinMenuDto();
			wmd.setKey(wm.getMenuKey());
			wmd.setName(wm.getName());
			wmd.setType(wm.getType());
			wmd.setId(wm.getId());
			wmd.setUrl(wm.getUrl());
			if(wm.getPid()==null||wm.getPid()==0) {
				if(wmd.getSub_button()==null) {
					wmd.setSub_button(new ArrayList<WeixinMenuDto>());
				}
				menuDtos.add(wmd);
			} else {
				WeixinMenuDto twmd = findById(wm.getPid(), menuDtos);
				if(twmd==null) throw new RuntimeException("菜单的父类对象有问题，请检查");
				twmd.getSub_button().add(wmd);
			}
		}
		return menuDtos;
	}

	private WeixinMenuDto findById(int id, List<WeixinMenuDto> wmds) {
		for(WeixinMenuDto wmd:wmds) {
			if(wmd.getId()==id) {
				return wmd;
			}
		}
		return null;
	}

}
