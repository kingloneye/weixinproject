package org.kinglone.basic.service;

import java.util.List;

import org.kinglone.basic.model.WeixinMenu;
import org.kinglone.weixin.model.WeixinMenuDto;

public interface WeixinMenuService{

	public void add(WeixinMenu wm);

	public void delete(int id);

	public void update(WeixinMenu wm);

	public WeixinMenu load(int id);

	public List<WeixinMenu> listAll();

	public WeixinMenu loadByKey(String key);

	public List<WeixinMenuDto> generateWeixinMenuDto();
	
	//private WeixinMenuDto findById(int id,List<WeixinMenuDto> wmds);
}
