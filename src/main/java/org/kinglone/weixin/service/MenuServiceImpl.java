package org.kinglone.weixin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.kinglone.basic.service.WeixinMenuService;
import org.kinglone.weixin.kit.WeixinBasicKit;
import org.kinglone.weixin.model.WeixinFinalValue;
import org.kinglone.weixin.model.WeixinMenuDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("menuService")
@Transactional(readOnly=false)
public class MenuServiceImpl implements MenuService{

	@Autowired
	private WeixinMenuService weixinMenuService;
	
	@Override
	public void publishMenu() {
		String url = WeixinBasicKit.replaceAccessTokenUrl(WeixinFinalValue.MENU_ADD);
		System.out.println(url);
		List<WeixinMenuDto> wmds = weixinMenuService.generateWeixinMenuDto();
		Map<String,List<WeixinMenuDto>> maps = new HashMap<String,List<WeixinMenuDto>>();
		maps.put("button", wmds);
		String json = JSONObject.fromObject(maps).toString();
	//	String json =  JsonUtil.getInstance().obj2json(maps);
		System.out.println(json);
		String rel = WeixinBasicKit.sendJsonPost(url,json);
		/*if(!WeixinBasicKit.checkRequestSucc(rel)) {
			throw new RuntimeException("发布菜单失败："+WeixinBasicKit.getRequestMsg(rel));
		}*/
		
	}

	@Override
	public String queryMenu() {
		String url = WeixinBasicKit.replaceAccessTokenUrl(WeixinFinalValue.MENU_QUERY);
		return WeixinBasicKit.sendGet(url);
	}

}
