package org.kinglone.weixin.service;

public interface MenuService {

	public void publishMenu();

	public String queryMenu();

}
