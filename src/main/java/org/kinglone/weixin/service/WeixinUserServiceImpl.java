package org.kinglone.weixin.service;

import net.sf.json.JSONObject;

import org.kinglone.weixin.kit.WeixinBasicKit;
import org.kinglone.weixin.model.WeixinFinalValue;
import org.kinglone.weixin.model.WeixinUser;
import org.kinglone.weixin.web.servlet.WeixinContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("weixinUserService")
@Transactional(readOnly=false)
public class WeixinUserServiceImpl implements WeixinUserService{
	@Override
	public WeixinUser queryByOpenid(String openid) {
		String url = WeixinBasicKit.replaceAccessTokenUrl(WeixinFinalValue.USER_QUERY);
		url = url.replace("OPENID", openid);
		String json = WeixinBasicKit.sendGet(url);
		System.out.println(json);
		JSONObject jsonObject=JSONObject.fromObject(json);
		//return (WeixinUser)JsonUtil.getInstance().json2obj(json, WUser.class);
		WeixinUser weixinUser = (WeixinUser) JSONObject.toBean(jsonObject, WeixinUser.class);
		return weixinUser;
	}

	@Override
	public String queryOpenidByCode(String code)  {
		String url = WeixinFinalValue.AUTH_GET_OID;
		url = url.replace("APPID", WeixinContext.getInstance().getAppId())
		   .replace("SECRET",WeixinContext.getInstance().getAppSecurt())
		   .replace("CODE", code);
		String json = WeixinBasicKit.sendGet(url);
		System.out.println("---------json:"+json);
		//String openid = JsonUtil.getMapper().readTree(json).get("openid").asText();
		String openid = "";
		return openid;
		//return null;
	}
}
