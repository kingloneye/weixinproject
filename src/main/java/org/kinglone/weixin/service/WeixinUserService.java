package org.kinglone.weixin.service;

import org.kinglone.weixin.model.WeixinUser;

public interface WeixinUserService {
	public WeixinUser queryByOpenid(String openid);
	public String queryOpenidByCode(String code);
}
