package org.kinglone.weixin.controller;

import org.kinglone.basic.model.WeixinMenu;
import org.kinglone.basic.service.WeixinMenuService;
import org.kinglone.weixin.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/weixinMenu")
public class WeixinMenuController {

	@Autowired
	private WeixinMenuService weixinMenuService;
	
	@Autowired
	private MenuService menuService;
	
	/**
	 * 列表
	 * @param model
	 * @return
	 */
	@RequestMapping("/list")
	public String list(Model model){
		model.addAttribute("menus", weixinMenuService.listAll());
		model.addAttribute("wmds", weixinMenuService.generateWeixinMenuDto());
		return "weixinMenu/list";
	}
	/**
	 * 跳转添加页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model){	
		model.addAttribute("menu", new WeixinMenu());
		return "weixinMenu/add";
	}
	/**
	 * 提交添加
	 * @param weixinMenu
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(WeixinMenu weixinMenu){
		weixinMenuService.add(weixinMenu);
		return "redirect:/weixinMenu/list";	
	}
	
	/**
	 * 跳转跟新页面
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/update/{id}",method=RequestMethod.GET)
	public String update(@PathVariable int id,Model model){
		model.addAttribute("menu", weixinMenuService.load(id));
		return "weixinMenu/add";
	}
	/**
	 * 跟新
	 * @param id
	 * @param weixinMenu
	 * @return
	 */
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public String update(@PathVariable int id,WeixinMenu weixinMenu){
		WeixinMenu twm = weixinMenuService.load(id);
		twm.setContent(weixinMenu.getContent());
		twm.setMenuKey(weixinMenu.getMenuKey());
		twm.setName(weixinMenu.getName());
		twm.setRespType(weixinMenu.getRespType());
		twm.setType(weixinMenu.getType());
		twm.setUrl(weixinMenu.getUrl());
		weixinMenuService.update(twm);
		return "redirect:/weixinMenu/list";
	}
	
	/**
	 * 跳转跟新页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/queryPublishMenu",method=RequestMethod.GET)
	public String queryPublish(Model model) {
		model.addAttribute("ms", menuService.queryMenu());
		return "weixinMenu/publish";
	}
	
	@RequestMapping("/publishMenu")
	public String publishMenu() {
		menuService.publishMenu();
		return "redirect:/weixinMenu/queryPublishMenu";
	}
	
}
