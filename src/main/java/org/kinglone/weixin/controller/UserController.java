package org.kinglone.weixin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.kinglone.basic.model.User;
import org.kinglone.basic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/user")
@Controller
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping("/list")
	public String list(Model model) {
		model.addAttribute("users", userService.list());
		return "user/list";	
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String add(Model model) {
		model.addAttribute("user", new User());
		return "user/add";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String add(User user) {
		user.setBind(0);
		userService.add(user);
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.GET)
	public String update(@PathVariable int id,Model model) {
		User u = userService.load(id);
		model.addAttribute("user",u);
		return "user/update";
	}
	
	@RequestMapping(value="/update/{id}",method=RequestMethod.POST)
	public String update(@PathVariable int id,User user) {
		User tu = userService.load(id);
		tu.setNickname(user.getNickname());
		tu.setPassword(user.getPassword());
		userService.update(tu);
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String delete(@PathVariable int id) {
		userService.delete(id);
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login() {
		return "user/login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String username,String password,HttpSession session) {
		User u = userService.login(username, password);
		session.setAttribute("user", u);
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/bindNewUser",method=RequestMethod.GET)
	public String bindNewUser(Model model,HttpSession session,HttpServletResponse resp) throws IOException {
		User u = (User)session.getAttribute("user");
		if(u==null) resp.sendRedirect("/user/login");
		System.out.println(u.getBind());
		if(u.getBind()==1) return "redirect:/user/list";
		model.addAttribute("user", u);
		return "user/bindNewUser";
	}
	
	
	@RequestMapping(value="/bindNewUser",method=RequestMethod.POST)
	public String bindNewUser(String username,String password,HttpSession session,HttpServletResponse resp) throws IOException {
		User u = (User)session.getAttribute("user");
		if(u==null) resp.sendRedirect("/user/login");
		
		System.out.println(username);
		User tu = userService.load(u.getId());
		tu.setBind(1);
		tu.setUsername(username);
		tu.setPassword(password);
		userService.update(tu);
		session.setAttribute("user", tu);
		return "redirect:/user/list";
	}
	
	@RequestMapping(value="/bindExistUser",method=RequestMethod.GET)
	public String bindExistUser(Model model,HttpSession session,HttpServletResponse resp) throws IOException {
		User u = (User)session.getAttribute("user");
		if(u==null) resp.sendRedirect("/user/login");
		if(u.getBind()==1) return "redirect:/user/list";
		return "user/bindExistUser";
	}
	
	@RequestMapping(value="/bindExistUser",method=RequestMethod.POST)
	public String bindExistUser(String username,String password,Model model,HttpSession session,HttpServletResponse resp) throws IOException {
		User u = (User)session.getAttribute("user");
		if(u==null) resp.sendRedirect("/user/login");
		User tu = userService.loadByUsername(username);
		if(tu.getPassword().equals(password)) {
			tu.setBind(1);
			tu.setImgUrl(u.getImgUrl());
			tu.setSex(u.getSex());
			tu.setStatus(1);
			tu.setNickname(u.getNickname());
			tu.setOpenid(u.getOpenid());
			userService.update(tu);
			userService.delete(u.getId());
			session.setAttribute("user", tu);
		} else {
			throw new RuntimeException("原始的密码输入出错!");
		}
		return "redirect:/user/list";
	}
	
}
