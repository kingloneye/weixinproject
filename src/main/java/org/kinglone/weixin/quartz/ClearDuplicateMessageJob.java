package org.kinglone.weixin.quartz;

import org.kinglone.weixin.kit.DuplicateMessageKit;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ClearDuplicateMessageJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		DuplicateMessageKit.clear();
	}

}
