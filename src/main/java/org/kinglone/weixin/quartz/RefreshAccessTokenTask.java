package org.kinglone.weixin.quartz;

import net.sf.json.JSONObject;

import org.kinglone.weixin.json.AccessToken;
import org.kinglone.weixin.kit.WeixinBasicKit;
import org.kinglone.weixin.model.WeixinFinalValue;
import org.kinglone.weixin.web.servlet.WeixinContext;
import org.springframework.stereotype.Component;
/**
 * 刷新token，需要执行的代码
 * @author Administrator
 *
 */
@Component
public class RefreshAccessTokenTask {
	
	public void refreshToken() {
		
		String url = WeixinFinalValue.ACCESS_TOKEN_URL;
		url = url.replaceAll("APPID", WeixinContext.getInstance().getAppId());
		url = url.replaceAll("APPSECRET", WeixinContext.getInstance().getAppSecurt());
		System.out.println(url);		
		String content = WeixinBasicKit.sendGet(url);	
		try {
			 //1、使用JSONObject
	        JSONObject jsonObject=JSONObject.fromObject(content);
	        AccessToken at=(AccessToken)JSONObject.toBean(jsonObject, AccessToken.class); 
	        WeixinContext.getInstance().setAccessToken(at);
			System.out.println("token为:"+at.getAccess_token()+"--有效期为："+at.getExpires_in());	
		} catch (Exception e) {
			e.printStackTrace();
			refreshToken();
		}
							
	}
}
