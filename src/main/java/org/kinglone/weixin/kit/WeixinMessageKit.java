package org.kinglone.weixin.kit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

public class WeixinMessageKit {
	
	public static String handlerReceiveMsg(HttpServletRequest req) throws IOException {
		Map<String,String> msgMap = reqMsg2Map(req);
		System.out.println(msgMap);
		if(!DuplicateMessageKit.checkDuplicate(msgMap)) {
			String rel = DuplicateMessageKit.getRel(msgMap);
			return rel;
		}
		String msgType = msgMap.get("MsgType");
		if("event".equals(msgType.trim())) {
			String rel = WeixinEventKit.handlerEventMsg(msgMap);
			DuplicateMessageKit.setRel(msgMap, rel);
			return rel;
		} else {
			return hanlderCommonMsg(msgMap);
		}
		
	}

	private static String hanlderCommonMsg(Map<String, String> msgMap) {
		return null;
	}

	/**
	 * 2.将解析的xml消息存到map中
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,String> reqMsg2Map(HttpServletRequest req) throws IOException {
		//2.读取xml文件
		String xml = req2xml(req);
		try {
			//将得到的消息存到map中
			Map<String,String> maps = new HashMap<String, String>();
			//将xml文件转换成document对象
			Document document = DocumentHelper.parseText(xml);
			//得到rootElement
			Element root = document.getRootElement();
			//得到所有的元素
			List<Element> eles = root.elements();
			for(Element e:eles) {
				maps.put(e.getName(), e.getTextTrim());
			}
			return maps;
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 1.读取xml文件
	 * @param req
	 * @return
	 * @throws IOException
	 */
	private static String req2xml(HttpServletRequest req) throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(req.getInputStream()));
		String str = null;
		StringBuffer sb = new StringBuffer();
		while((str=br.readLine())!=null) {
			sb.append(str);
		}
		return sb.toString();
	}
	/**
	 * 5.将map转换成xml文件
	 * @param map
	 * @return
	 * @throws IOException
	 */
	public static String map2xml(Map<String, Object> map) throws IOException {
		Document d = DocumentHelper.createDocument();
		Element root = d.addElement("xml");
		Set<String> keys = map.keySet();
		for(String key:keys) {
			Object o = map.get(key);
			if(o instanceof String) {
				String t = (String)o;
//				System.out.println(t);
				if(t.indexOf("<a")>=0) {
					root.addElement(key).addCDATA(t); 
				} else {
					root.addElement(key).addText((String)o);
				}
			} else {
				
			}
		}
		StringWriter sw = new StringWriter();
		XMLWriter xw = new XMLWriter(sw);
		xw.setEscapeText(false);
		xw.write(d);
		return sw.toString();
	}
}
